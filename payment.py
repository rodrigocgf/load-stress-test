import json
import sys
import threading
import queue
import requests
import logger
import os
import time
import requests
import logger
from jsonmerge import merge


class Payment(object):

    def __init__(self , strMode, strNumberThrs, strNumberPayload):
        sCurDir = os.getcwd()
        self.LogsDir = os.path.abspath( sCurDir)
        self.log = logger.Logger(os.path.join(self.LogsDir,"logs") , "Payment")

        self.log.info("===================================================================")
        self.log.info(" ")
        self.payload_title_amount = 100.0

        self.strMode = strMode
        self.iNumberThrs = int(strNumberThrs)
        self.iNumberPayload = int(strNumberPayload)

         
        self.log.info("[Payment] Number of threads : {}".format(self.iNumberThrs))
        self.log.info("[Payment] Number of payloads : {}".format(self.iNumberPayload))


    def Start(self):
        self.bEnd = False
        if self.strMode == "1":
            self.thr = threading.Thread(target = self.Loop)
        else:
            self.thr = threading.Thread(target = self.LoopMultithread)
        self.thr.start()
        self.log.info("Starting Payment ...")

    def LoopMultithread(self):
        access_token = self.GetAccessToken()

        for i in range(self.iNumberThrs):
            self.payload_title_amount += 100
            self.thrSend = threading.Thread(target = self.ThrSend, args=(self.log, access_token ))
            self.thrSend.start()
            self.log.info("Starting Payment " + str(i))

    def ThrSend(self, pLog, pAccessToken):
        message = self.MountMultiplePayments(self.iNumberPayload)
        self.SendHttpPostMessage(message, pAccessToken)
        pLog.info("----> HTTP JSON MESSAGE sent to AWS API GATEWAY.")


    def Stop(self):
        self.log.info("Stopping Payment ...")
        self.bEnd = True
        self.thr.join()

    #
    # SEQUENTIAL SEND
    #
    def Loop(self):
        access_token = self.GetAccessToken()

        while self.bEnd == False:
            message = self.MountSamplePayment()
            self.SendHttpPostMessage(message, access_token)
            self.log.info("----> HTTP JSON MESSAGE sent to AWS API GATEWAY.")
            time.sleep(0.1)

    def GetAccessToken(self):
        URL_LOGIN = "http://openbanking-homol.finnet.com.br/v1/login"
        jsonAuth = {"username": "rodrigo.franca","password": "finnet10@"}
        headers = {'Content-Type': 'application/json'} 
        rlogin = requests.post(URL_LOGIN, json=jsonAuth, headers=headers )
        self.log.info("[Payment::GetAccessToken] login status code : " + str(rlogin.status_code) )
        self.log.info("[Payment::GetAccessToken] login reason : " + str(rlogin.reason) )
        self.log.info("[Payment::GetAccessToken] login text : " + str(rlogin.text) )

        jsonLogin = json.loads(rlogin.text)

        self.log.info("[Payment::GetAccessToken] acess token : " + str(jsonLogin) )

        return jsonLogin['access_token']


    def SendHttpPostMessage(self, pMessage, pAccessToken):
        try:
            headers = {'authorization': str(pAccessToken) } 
            URL = "https://openbanking-homol.finnet.com.br/v1/payment"
            
            r = requests.post(URL, json=pMessage, headers=headers )

            self.log.info("[Payment::SendHttpPostMessage] status code : " + str(r.status_code) )
            self.log.info("[Payment::SendHttpPostMessage] reason : " + str(r.reason) )
            self.log.info("[Payment::GetAccessToken] login text : " + str(r.text) )

        except requests.ConnectionError as ex:
            self.log.info("[Payment::SendHttpPostMessage] https post exception :" + str(ex))


    def MountSamplePayment(self):
        message = {
            "company": {
                "bank_identifier": 341,
                "bank_branch": 1234,
                "bank_branch_identifier": "5",
                "bank_account": 6789012,
                "bank_account_identifier": "3",
                "bank_agreement": "45678",
                "registered_number": 48235916000157,
                "type": 1,
                "name": "PAGADOR TESTE",
                "address_zip_code": 9876543,
                "address": "RUA TESTE",
                "address_number": 123,
                "address_complement": "456",
                "address_neighborhood": "BAIRRO TESTE",
                "address_city": "SAO PAULO",
                "address_state": "SP"
            },
            "payments": [
                {
                    "payment": {
                        "type": "DARF_SIMPLES",
                        "identifier": "111111118",
                        "date": "2019-11-04",
                        "amount": self.payload_title_amount,
                        "due_date": "2019-11-05",
                        "tax_code": 1234,
                        "tax_calculation_period": "2019-11-01",
                        "gross_revenue": 13.13,
                        "gross_revenue_percentage": 10.00,
                        "original_amount": 13.13,
                        "penalty_amount": 11.11,
                        "interest_amount": 12.12,
                        "bank_authentication": ""
                    },
                    "taxpayer": {
                        "type": 1,
                        "identifier": 12345678901,
                        "name": "CONTRIBUINTE TESTE DARFS"
                    }
                }
            ]
        }

        return message

    def MountMultiplePayments(self, pNumber):
        company = {
            "company": {
                "bank_identifier": 341,
                "bank_branch": 1234,
                "bank_branch_identifier": "5",
                "bank_account": 6789012,
                "bank_account_identifier": "3",
                "bank_agreement": "45678",
                "registered_number": 48235916000157,
                "type": 1,
                "name": "PAGADOR TESTE",
                "address_zip_code": 9876543,
                "address": "RUA TESTE",
                "address_number": 123,
                "address_complement": "456",
                "address_neighborhood": "BAIRRO TESTE",
                "address_city": "SAO PAULO",
                "address_state": "SP"
            }
        }

        arrPayments = "{\"payments\" : [ "
        for j in range(pNumber):
            self.payload_title_amount += 100
            payment = {
                    "payment": {
                        "type": "DARF_SIMPLES",
                        "identifier": "111111118",
                        "date": "2019-11-04",
                        "amount": self.payload_title_amount,
                        "due_date": "2019-11-05",
                        "tax_code": 1234,
                        "tax_calculation_period": "2019-11-01",
                        "gross_revenue": 13.13,
                        "gross_revenue_percentage": 10.00,
                        "original_amount": 13.13,
                        "penalty_amount": 11.11,
                        "interest_amount": 12.12,
                        "bank_authentication": ""
                    },
                    "taxpayer": {
                        "type": 1,
                        "identifier": 12345678901,
                        "name": "CONTRIBUINTE TESTE DARFS"
                    }
                }

            arrPayments = arrPayments + str(json.dumps(payment))
            if j < (pNumber - 1):
                arrPayments = arrPayments + ", "

        arrPayments = arrPayments + "]}"
            
        payload = {}
        payload = merge(company,json.loads(arrPayments) )


        self.log.info("payload : " + str(json.dumps(payload)))

        return payload
