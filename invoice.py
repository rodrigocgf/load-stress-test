import json
import sys
import threading
import queue
import requests
import logger
import os
import time
import requests
import logger
from jsonmerge import merge


class Invoice(object):

    def __init__(self, strMode, strNumberThrs, strNumberPayload):
        sCurDir = os.getcwd()
        self.LogsDir = os.path.abspath( sCurDir)
        self.log = logger.Logger(os.path.join(self.LogsDir,"logs") , "Invoice")

        self.log.info("===================================================================")
        self.log.info(" ")
        self.payload_title_amount = 100.0
        self.identifier = 100

        self.strMode = strMode
        self.iNumberThrs = int(strNumberThrs)
        self.iNumberPayload = int(strNumberPayload)

         
        self.log.info("[Payment] Number of threads : {}".format(self.iNumberThrs))
        self.log.info("[Payment] Number of payloads : {}".format(self.iNumberPayload))

    def Start(self):
        self.bEnd = False
        if self.strMode == "1":
            self.thr = threading.Thread(target = self.Loop)
        else:
            self.thr = threading.Thread(target = self.LoopMultithread)
        self.thr.start()
        self.log.info("Starting Invoice ...")

    def LoopMultithread(self):
        access_token = self.GetAccessToken()

        for i in range(self.iNumberThrs):
            self.payload_title_amount += 100
            self.thrSend = threading.Thread(target = self.ThrSend, args=(self.log, access_token ))
            self.thrSend.start()
            self.log.info("Starting Invoice " + str(i))

    def ThrSend(self, pLog, pAccessToken):
        message = self.MountMultipleInvoices(self.iNumberPayload)
        self.SendHttpPostMessage(message, pAccessToken)
        pLog.info("----> HTTP JSON MESSAGE sent to AWS API GATEWAY.")

    def Stop(self):
        self.log.info("Stopping Invoice ...")
        self.bEnd = True
        self.thr.join()

    def Loop(self):
        access_token = self.GetAccessToken()

        while self.bEnd == False:
            message = self.MountSampleInvoice()
            self.SendHttpPostMessage(message, access_token)
            self.log.info("----> HTTP JSON MESSAGE sent to AWS API GATEWAY.")
            time.sleep(0.1)

    def GetAccessToken(self):
        URL_LOGIN = "http://openbanking-homol.finnet.com.br/v1/login"
        jsonAuth = {"username": "rodrigo.franca","password": "finnet10@"}
        headers = {'Content-Type': 'application/json'} 
        rlogin = requests.post(URL_LOGIN, json=jsonAuth, headers=headers )
        self.log.info("[Invoice::GetAccessToken] login status code : " + str(rlogin.status_code) )
        self.log.info("[Invoice::GetAccessToken] login reason : " + str(rlogin.reason) )
        self.log.info("[Invoice::GetAccessToken] login text : " + str(rlogin.text) )

        jsonLogin = json.loads(rlogin.text)

        self.log.info("[Invoice::GetAccessToken] acess token : " + str(jsonLogin) )

        return jsonLogin['access_token']

    def SendHttpPostMessage(self, pMessage, pAccessToken):
        try:
            headers = {'authorization': str(pAccessToken) } 
            URL = "https://openbanking-homol.finnet.com.br/v1/payment"
            
            r = requests.post(URL, json=pMessage, headers=headers )

            self.log.info("[Invoice::SendHttpPostMessage] status code : " + str(r.status_code) )
            self.log.info("[Invoice::SendHttpPostMessage] reason : " + str(r.reason) )
            self.log.info("[Invoice::GetAccessToken] login text : " + str(r.text) )

        except requests.ConnectionError as ex:
            self.log.info("[Invoice::SendHttpPostMessage] https post exception :" + str(ex))

    def MountSampleInvoice(self):
        message = {
                "company": {
                    "type": 1,
                    "registered_number": 84438011000148,
                    "name" : "FINNET",
                    "bank_identifier": 104,
                    "bank_branch": 1111,
                    "bank_branch_identifier": "2",
                    "bank_account": 2222222,
                    "bank_account_identifier": "3",
                    "bank_agreement": 444444
                },
                "invoices" : [
                    {
                        "invoice" : {
                            "transaction_code" : 10 , 
                            "bank_wallet_code" : 3, 
                            "bank_slip_emission_id" : 5, 
                            "bank_slip_delivery_id" : "3", 
                            "identifier" : "4624177", 
                            "title_due_date" : "2020-11-15", 
                            "title_amount" : self.payload_title_amount, 
                            "title_type" : 22, 
                            "title_issue_date" : "2020-12-12"
                        },
                        "payer" : {
                            "registration_type" : 1, 
                            "registration_number" : 84438011000148, 
                            "name" : "FINNET", 
                            "address" : "RUA FORMOSA, 145", 
                            "neighborhood" : "JARDIM PAULISTA", 
                            "zip_code" : "10100001", 
                            "city" : "SAO PAULO", 
                            "state" : "SP"
                        }
                    }
                ]
            }

        return message


    def MountMultipleInvoices(self, pNumber):
        company = {
            "company": {
                "type": 1,
                "registered_number": 84438011000148,
                "name" : "FINNET",
                "bank_identifier": 104,
                "bank_branch": 1111,
                "bank_branch_identifier": "2",
                "bank_account": 2222222,
                "bank_account_identifier": "3",
                "bank_agreement": 444444
            }
        }
            
        arrInvoices = "{\"invoices\" : [ "
        for j in range(pNumber):
            self.identifier += 100
            self.payload_title_amount += 100
            invoice = {
                    "invoice" : {
                        "transaction_code" : 10 , 
                        "bank_wallet_code" : 3, 
                        "bank_slip_emission_id" : 5, 
                        "bank_slip_delivery_id" : "3", 
                        "identifier" : str(self.identifier), 
                        "title_due_date" : "2020-11-15", 
                        "title_amount" : self.payload_title_amount, 
                        "title_type" : 22, 
                        "title_issue_date" : "2020-12-12"
                    },
                    "payer" : {
                        "registration_type" : 1, 
                        "registration_number" : 84438011000148, 
                        "name" : "FINNET", 
                        "address" : "RUA FORMOSA, 145", 
                        "neighborhood" : "JARDIM PAULISTA", 
                        "zip_code" : "10100001", 
                        "city" : "SAO PAULO", 
                        "state" : "SP"
                    }
                }

            arrInvoices = arrInvoices + str(json.dumps(invoice))
            if j < (pNumber - 1):
                arrInvoices = arrInvoices + ", "

        arrInvoices = arrInvoices + "]}"
            
        payload = {}
        payload = merge(company,json.loads(arrInvoices) )


        self.log.info("payload : " + str(json.dumps(payload)))

        return payload