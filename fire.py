import sys
import threading
from controller import Controller as Controller
import time
import os

def main():
    sCurDir = os.getcwd()
    LogsDir = os.path.abspath( sCurDir)

    thrMain = threading.Thread( target = LoopMain, args=(LogsDir,))
    thrMain.start()

def LoopMain(pLogsDir):
    strInput = ""
    strChoice = ""
    strMode = ""
    strNumberThrs = ""
    strNumberPayload = ""

    sys.stdout.write("***************************************************************************************\r\n")
    sys.stdout.write("\tLogs directory : " + pLogsDir + "\r\n")
    sys.stdout.write("***************************************************************************************\r\n")

    print("Enter test type : ")
    print("    1 - Payment   ")
    print("    2 - Invoice   ")
    print("> ")
    strChoice = sys.stdin.readline()
    strChoice = strChoice.lstrip().rstrip()
    print(" ")

    print("Enter mode : ")
    print("    1 - Sequential sending")
    print("    2 - Simultaneous parallel sending")
    print("> ")
    strMode = sys.stdin.readline()
    strMode = strMode.lstrip().rstrip()

    if strMode == "2":
        print("Enter the number of sending threads : ")
        print("> ")
        strNumberThrs = sys.stdin.readline()
        strNumberThrs = strNumberThrs.lstrip().rstrip()
        print("Enter the number of payloads : ")
        print("> ")
        strNumberPayload = sys.stdin.readline()
        strNumberPayload = strNumberPayload.lstrip().rstrip()

    print(" ")
    print("Going to start : " + str(strChoice))

    controller = Controller(str(strChoice), pLogsDir, strMode, strNumberThrs, strNumberPayload)
    controller.Start()

    while strInput != "quit":
        print("Enter 'quit' to exit : ")
        strInput = sys.stdin.readline()
        strInput = strInput.lstrip().rstrip()

        if ( str(strInput) == "quit" ):
            controller.Stop()

if __name__ == "__main__":
    sys.exit(main() or 0)