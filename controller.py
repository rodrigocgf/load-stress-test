import json
import sys
import threading
import queue
import requests
import logger
import os
import time
from payment import Payment as Payment
from invoice import Invoice as Invoice

class Controller(object):

    def __init__(self, testType, osLogsDir,strMode, strNumberThrs, strNumberPayload):
        self.OsLogsDir = str(osLogsDir)

        self.log = logger.Logger(os.path.join(self.OsLogsDir,"logs"),"Controller")
        self.log.propagete = False
        self.TestType = testType

        self.strMode = strMode
        self.strNumberThrs = strNumberThrs
        self.strNumberPayload = strNumberPayload

        self.bEnd = False

    def Start(self):
        self.log.info("===================================================================")
        self.log.info(" ")
        self.log.info("Starting Controller ...")

        if self.TestType == "1":
            self.thrPayment = threading.Thread(target = self.LoopPayment)
            self.thrPayment.daemon = True
            self.thrPayment.start()

        elif self.TestType == "2":
            self.thrInvoice = threading.Thread(target = self.LoopInvoice)
            self.thrInvoice.daemon = True
            self.thrInvoice.start()

    def LoopPayment(self):
        payment = Payment(self.strMode, self.strNumberThrs, self.strNumberPayload)
        payment.Start()

        while self.bEnd == False:
            time.sleep(100)

        payment.Stop()


    def LoopInvoice(self):

        invoice = Invoice(self.strMode, self.strNumberThrs, self.strNumberPayload)
        invoice.Start()

        while self.bEnd == False:
            time.sleep(100)

        invoice.Stop()


    def Stop(self):
        self.bEnd = True

        self.log.info("Controller stopped.")
        self.log.info(" ")
        self.log.info("===================================================================")
        